const samples_desc = {
  polkadot:
    "Polkadot positions itself as the next-generation blockchain protocol, capable of connecting multiple specialized chains into one universal network.",
  cardano:
    "Cardano is a Proof of Stake blockchain project that has yet to reach its full potential. As a “third-generation” blockchain, it seeks to address the scalability issues inherent in second-generation blockchains.",
  chainlink:
    "Chainlink has become one of the most commonly used projects in the cryptocurrency space. It’s a decentralized oracle service that can provide external data to smart contracts on Ethereum. In other words, it connects blockchains with the real world.",
  xrp:
    "The main goal of XRP is to connect banks, payment providers and digital asset exchanges, enabling faster and cost-efficient global payments.",
  ethereum:
    "Ethereum is a decentralized computing platform. You can think of it like a laptop or PC, but it doesn't run on a single device. Instead, it simultaneously runs on thousands of machines around the world, meaning that it has no owner.",
  litecoin:
    "Litecoin is a peer-to-peer cryptocurrency and open-source software project. It was an early bitcoin spinoff, starting in October 2011. In technical details, Litecoin is nearly identical to Bitcoin, although its transactions are faster and cheaper.",
};

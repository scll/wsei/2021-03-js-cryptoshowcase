const cs = document.getElementById("contentscreen");
const ws = document.getElementById("welcomescreen");
const rt = document.getElementById("root");
let carousel = ["welcomescreen", "cardano_w", "polkadot_w", "litecoin_w", "ethereum_w", "chainlink_w", "xrp_w"];
let active = 0;

rt.addEventListener("wheel", (e) => {
  e.preventDefault();
  let direction = e.deltaY > 0 ? "down" : "up";
  if (direction === "down") {
    active = active === carousel.length - 1 ? carousel.length - 1 : active + 1;
  } else if (direction === "up") {
    active = active ? active - 1 : 0;
  }
  let m = document.getElementById(carousel[active]);
  m.scrollIntoView({
    behavior: "smooth",
  });
});

window.innerHeight / window.innerWidth > 0.9 && ws.classList.add("vertical");
window.innerHeight / window.innerWidth > 0.84 && cs.classList.add("vertical");

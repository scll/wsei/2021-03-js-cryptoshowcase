const arrowbt = document.getElementById("arrowbtwrap");

arrowbt.addEventListener("click", () => {
  active = 1;
  window.scroll({ top: cs.offsetTop, left: 0, behavior: "smooth" });
});

const lw = document.getElementById("logoswrapper");
const cryptonames = ["polkadot", "cardano", "chainlink", "xrp", "ethereum", "litecoin"];

for (let i = 0; i < cryptonames.length; i++) {
  let logowrapper = document.createElement("div");
  logowrapper.classList = "cryptologowrapper";
  logowrapper.addEventListener("click", () => {
    active = carousel.indexOf(`${cryptonames[i]}_w`);
    document.getElementById(`${cryptonames[i]}_w`).scrollIntoView({
      behavior: "smooth",
    });
  });
  let logo = document.createElement("img");
  logo.classList = "cryptologo";
  logo.src = `./assets/images/${cryptonames[i]}.png`;
  logowrapper.appendChild(logo);
  lw.appendChild(logowrapper);
}

window.addEventListener("resize", () => {
  if (window.innerHeight / window.innerWidth > 0.9) {
    !ws.classList.value.includes("vertical") && ws.classList.add("vertical");
  } else if (window.innerHeight / window.innerWidth <= 0.9) {
    ws.classList.value.includes("vertical") && ws.classList.remove("vertical");
  }
});

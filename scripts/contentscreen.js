const createdivider = (has_s, id) => {
  let dividerwrap = document.createElement("div");
  dividerwrap.classList = "dividerwrap";
  id && (dividerwrap.id = id);
  if (has_s) {
    let dividerleft = document.createElement("div");
    dividerleft.classList = "dividerleft";
    let dividerright = document.createElement("div");
    dividerright.classList = "dividerright";
    dividerwrap.appendChild(dividerleft);
    dividerwrap.appendChild(dividerright);
  }
  return dividerwrap;
};

const createsample = (id, text) => {
  let samplewrapper = document.createElement("div");
  samplewrapper.classList = "cryptosample";
  samplewrapper.id = `${id}_s`;

  let logo = document.createElement("img");
  logo.classList = "cryptologo";
  logo.src = `./assets/images/${id}.png`;
  samplewrapper.appendChild(logo);

  let textwrap = document.createElement("div");
  textwrap.classList = "textwrap";
  let title = document.createElement("div");
  title.classList = "title";
  title.innerHTML = id;
  let desc = document.createElement("div");
  desc.classList = "desc";
  desc.innerHTML = text;
  let button = document.createElement("a");
  button.classList = "button";
  button.text = "Learn more";
  button.href = `./subpages/${id}/index.html`;

  textwrap.appendChild(title);
  textwrap.appendChild(desc);
  textwrap.appendChild(button);
  samplewrapper.appendChild(textwrap);
  return samplewrapper;
};

let crypto_w = [...carousel];
crypto_w = [...crypto_w.splice(1, crypto_w.length)];

for (let i = 0; i < crypto_w.length; i++) {
  let id = `${crypto_w[i].slice(0, crypto_w[i].length - 2)}`;
  let text = samples_desc[id];
  let has_s = i !== 0 && i !== crypto_w.length;
  cs.appendChild(createdivider(has_s, crypto_w[i]));
  cs.appendChild(createsample(id, text));
  i === crypto_w.length - 1 && cs.appendChild(createdivider());
}

window.addEventListener("resize", () => {
  if (window.innerHeight / window.innerWidth > 0.78) {
    !cs.classList.value.includes("vertical") && cs.classList.add("vertical");
  } else if (window.innerHeight / window.innerWidth <= 0.78) {
    cs.classList.value.includes("vertical") && cs.classList.remove("vertical");
  }
});

const resize_ob = new ResizeObserver((entries) => {
  let { width, height } = entries[0].contentRect;
  cs.style.backgroundSize = `${width}px ${height}px`;
});
resize_ob.observe(cs);

const rt = document.getElementById("root");
const cw = document.getElementById("contentwrap");

const createappbar = (nexttheme, name, color) => {
  let appbar = document.createElement("div");
  appbar.id = "appbar";
  appbar.style.background = nexttheme === "dark" ? color : "#222";

  let title = document.createElement("a");
  title.href = "../../index.html";
  title.text = "CryptoShowcase";
  let clipwrap = document.createElement("div");
  let clip = document.createElement("div");
  clipwrap.onclick = () => {
    document.getElementById("root").classList = nexttheme;
    init(name, nexttheme);
  };

  appbar.appendChild(title);
  clipwrap.appendChild(clip);
  appbar.appendChild(clipwrap);
  return appbar;
};

const createtitle = (n, s, c) => {
  let titlewrap = document.createElement("div");
  titlewrap.id = "titlewrap";

  let title = document.createElement("div");
  title.innerHTML = n;
  let symbol = document.createElement("div");
  symbol.innerHTML = `[<div style="color: ${c}; display: inline">${s}</div>]`;

  titlewrap.appendChild(title);
  titlewrap.appendChild(symbol);
  cw.appendChild(titlewrap);
};

const createsummary = (d, p, mc, c) => {
  let summarywrapper = document.createElement("div");
  summarywrapper.id = "cryptosummary";

  let date = document.createElement("div");
  date.innerHTML = `Date of launch: <i style="color: ${c}">${d}</i>`;
  let price = document.createElement("div");
  price.innerHTML = `Current price: <i style="color: ${c}">${p}</i>`;
  let marketcap = document.createElement("div");
  marketcap.innerHTML = `Market cap: <i style="color: ${c}">${mc}</i>`;

  summarywrapper.appendChild(date);
  summarywrapper.appendChild(price);
  summarywrapper.appendChild(marketcap);
  cw.appendChild(summarywrapper);
};

const createlogo = (name, theme) => {
  let logowrapper = document.createElement("div");
  logowrapper.id = "logowrapper";
  let logo = document.createElement("img");
  logo.id = "logo";
  const src = theme === "dark" ? `${name}-${theme}` : name;
  logo.src = `../../assets/images/${src}.png`;
  logowrapper.appendChild(logo);
  cw.appendChild(logowrapper);
};

const createp = (text) => {
  let p = document.createElement("p");
  p.innerHTML = text;
  cw.appendChild(p);
};

const createsources = (p, s, c) => {
  let sourceswrapper = document.createElement("div");
  sourceswrapper.id = "sourceswrapper";
  let page = document.createElement("div");
  page.innerHTML = `Official web page: <a style="color: ${c}" href='${p}' target='_blank'>${p}</a>`;
  let sourcecode = document.createElement("div");
  sourcecode.innerHTML = `Source code: <a style="color: ${c}" href='${s}' target='_blank'>${s}</a>`;
  sourceswrapper.appendChild(page);
  sourceswrapper.appendChild(sourcecode);
  cw.appendChild(sourceswrapper);
};

const createfooter = (color) => {
  let footer = document.createElement("div");
  footer.id = "footer";
  footer.innerHTML = "© 2021 CryptoShowcase";
  footer.style.color = color;
  return footer;
};

const init = (name, theme) => {
  rt.innerHTML = "";
  cw.innerHTML = "";
  let { date, symbol, price, marketcap, color, darkcolor, page, sourcecode, description } = main_crypto_data[name];
  const desc = description.split("newline");
  const currentcolor = theme === "dark" ? darkcolor : color;
  const nexttheme = theme === "dark" ? "light" : "dark";
  rt.appendChild(createappbar(nexttheme, name, color));
  createtitle(name, symbol, currentcolor);
  createsummary(date, price, marketcap, currentcolor);
  createp(desc[0]);
  createlogo(name, theme);
  createp(desc[1]);
  createp(desc[2]);
  createsources(page, sourcecode, currentcolor);
  rt.appendChild(cw);
  rt.appendChild(createfooter(currentcolor));
};
